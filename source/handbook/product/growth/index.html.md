---
layout: markdown_page
title: Growth
---

Because growth is a part of product we won't use the term growth in other parts of the company.
For example in marketing we might use demand generation and pipe-to-spend as a metric.

Growth stands for the growth in usage growth.

## SMAU

SMAU stands for Stage Monthly Active Users.
The growth group is responsbile for increasing these.

SMAU can be increased in multiple ways:

1. Increase usage pings
1. Increase new users
1. Increase retention of users
1. Increase reactivation of users
1. Increase number of stages per user
1. Increase the number of stages

Each of these is worthwhile to increase for their own sake:

1. More insight in how our product is used
1. More people intruduced to the product
1. Happier users of the product
1. Winning people back
1. When people use more stages they are less likely to churn and more likely to buy
1. A more extensive application that addresses more use cases

Each of these has different ways of increasing it:

1. Cloud License Management (Sync) and registration for dependency scanning
1. Request an account when you can't self-signup, instead of always having to ask the admin make it for you.
1. Fix problems that churned users report.
1. Email churned users when features are introduced that they requested before churning.
1. In-product hints at relevant times to use a new stage.
1. Add a network security stage.
