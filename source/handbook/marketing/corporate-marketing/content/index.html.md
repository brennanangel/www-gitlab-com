---
layout: markdown_page
title: "Content"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Content Marketing

The Content Marketing team includes audience development, editorial oversight,
social marketing, and content strategy, development, and
operations. The Content Marketing team is responsible for the stewardship of
GitLab's audiences, users, customers, and partners' content needs, preferences and
perceptions of GitLab. Content marketing creates engaging, inspiring, and
relevant content, executing integrated content programs to deliver useful and
cohesive content experiences that build trust and preference for GitLab.

## Other related pages

- [GitLab blog](/handbook/marketing/blog)
- [Editorial calendar](/handbook/marketing/corporate-marketing/content/editorial-calendar/)
- [Editorial review checklist](/handbook/marketing/corporate-marketing/content/editorial-review-checklist/)
- [Social media guidelines](/handbook/marketing/social-media-guidelines/)
- [Content Hack Day](/handbook/marketing/corporate-marketing/content/content-hack-day)
- [Newsletters ](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter) (managed by Marketing Programs)
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Communication

### Chat
Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of new, published content
- `#blog` for questions regarding blog content
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

### Issue trackers
  - [General Content Marketing](https://gitlab.com/gitlab-com/marketing/general/boards/104871?=)
  - [Blog posts](https://gitlab.com/gitlab-com/blog-posts/boards/409030)

### Team
- **Erica Lindberg, Manager, Content Marketing**
  - Area of focus: Leadership, case studies
  - Contact: [@erica](https://gitlab.com/erica)
- **Rebecca Dodd, Managing Editor**
  - Area of focus: Blog endboss, editing blog posts
  - Contact: [@rebecca](https://gitlab.com/rebecca)
- **Emily von Hoffmann, Associate Social Marketing Manager**
  - Area of focus: Social media and Medium (50%), Net new content for GitLab blog (50%)
  - Contact: [@evhoffmann](https://gitlab.com/evhoffmann)
- **Aricka Flowers, Content Marketing Associate, Ops**
  - Area of focus: Writing net new content for GitLab blog (100%)
  - Contact: [@atflowers](https://gitlab.com/atflowers)
- **Suri Patel, Content Marketing Associate, Dev**
  - Area of focus: Writing new content for GitLab blog (100%)
  - Contact: [@spatel](https://gitlab.com/suripatel)

## Editorial Mission Statement

Empower and inspire software teams to adopt and evolve a DevOps workflow to collaborate better, be more productive, and ship faster by sharing insightful and actionable information, advice, and resources.

### Vision

Build the largest and most diverse community of cutting edge co-conspirators who are leading the way to define and create the next generation of software development practices.

### 2018 Core content strategy statement

The content we produce helps increase awareness of GitLab’s complete and single application with the goal of broadening our market share and increasing sales by providing informative and persuasive content that makes DevOps teams feel excited, curious, and confident so that they can adopt & integrate industry best practices into their workflow.

## Messaging for Verticals
When to develop vertical messaging: The key is to determine if an industry has a certain pain point that another industry does not share.  You need to describe the problem (using industry specific terminology, if necessary) and also how your product solves these problems for them. Additionally, you can create high-level messaging and then branch off; for example if multiple industries are very security conscious, create security focused marketing, and adapt to select high value verticals.

## Checklist for good content<a name="checklist"></a>

- Relevant
- Useful
- User or customer-centered
- Clear, Consistent, Concise
- Supported

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first.
1. If you need help finding relevant content to use, ask for help in the #content channel.
1. If the content you're looking for doesn't exist, open an issue in [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) and label it `blog post`. The content team will evaluate if it's likely do well on the blog, in which case we will write the content. If the suggestion isn't likely do well, we will suggest you write it and we will help edit it.
1. If you are creating your own content and need a copy review, ping @erica. Please give at least 3 days notice.

## Writing blog posts

See the [blog handbook](/handbook/marketing/blog/) for detailed instructions on how to publish a blog post.

The Content Marketing team is responsible for increasing sessions on the GitLab blog month over month. We use data-drive insights to decide what to write on using the [content marketing dashboard](https://datastudio.google.com/u/0/reporting/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb/page/1M). In order to hit our goals, we aim to publish at least 3 blog posts that will garner 10,000+ sessions.

### Trends

*From [2018 blog analysis](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/248) conducted in 2018-10

1. Average sessions per month on the blog: ~30,000
1. Average sessions per post in published month: 3,792
1. Average sessions per content marketing post in published month: 3,500
1. 55% of posts get <1,000 sessions in a month
1. 27.47% of posts hit our expected outcome of 1,000-4,999 sessions in published month
1. 28.57% of posts garner less than 499 sessions in published month
1. 9% of posts are "hits" (10K+ sessions in published month); "Hits" don't consistently perform well over time

**Breakdown by category of "hits":**

- Content: 37.5%
- Product: 31.35%
- Corporate: 25%

### Obeservations

1. There is not a strong correlation between # of sessions and topic
1. Strong performing content marketing posts focus on show and tell engineering stories
1. Posts really fall into the 5,000-9,999 session bracket (3.3%)
1. Content hack day posts tend to perform well (>1,000 sessoins in published month)

### Identifying and qualifying a high-performing blog post

**Qualifying story ideas:**

Look for the following patterns:

1. Team implementing a new techonology, process, or coding language
1. Deep dive into how a popular feature is made
1. Chronicling a performance improvement
1. Covering a controversial decision that was made

**Who and how to interview:**

1. Contact the technical subject matter expert. This can be someone who created the issue, or managed the project.
1. Set up a 30 minute interview and dig into:
  - What was the challenge?
  - What was the solution?
  - How did you go from point a to point b? Walk me through your thought process.
  - How was the solution implemented and what is a realistic use case of the solution?
  - What lessons were learned?
  - How did this make the GitLab product / development community better?
1. Optional: Contact the business subject matter expert. This could be a product manager or a product marketing manager.
1. Set up a 30 minute interview and dig into:
  - How does this solution help a user?
  - What business value does this solution bring?
  - How does this solution relate to our product?

### Attributes of a successful blog post:

1. Deep dive into a hard technical challenge.
1. Puts the reader in the shoes of the person who faced the challenge; reader learns via compelling example.
1. Intellectually satisfying; learning compontent.
1. Allows the reader to learn from someone else's mistake, and follows a problem/trials and triumphs/solution story arch. 
1. Taking a controversial or unpopular stance on a topic, back by hard evidence. 

### Examples of high-performing posts (20K+ sessions in published month):

1. [Meet the GitLab Web IDE](/2018/06/15/introducing-gitlab-s-integrated-development-environment/)
1. [How a fix in Go 1.9 sped up our Gitaly service by 30x](/2018/01/23/how-a-fix-in-go-19-sped-up-our-gitaly-service-by-30x/)
1. [Hey, data teams - We're working on a tool just for you](/2018/08/01/hey-data-teams-we-are-working-on-a-tool-just-for-you/index.html)
1. [Why we chose Vue.js](https://about.gitlab.com/2016/10/20/why-we-chose-vue/)
1. [How we do Vue: one year later](/2017/11/09/gitlab-vue-one-year-later/)

### Conducting a blog analysis

Pull information on all blog posts for document how many sessions each post received in the month, and how many sessions they received of all time. Categorize them by type, bracket, total sessions in month, total sessions to date, category, theme, and topic. Eventually add first touch point revenue data.
Search Google Drive for `Blog Performance` to find the appriopriate sheet to work from.

- Blog post links should be added as they are published and category, audience, theme, and topic should be filled out.
- The Managing Editor and Manager, Content Marketing should review last month on the 1st of the month to fill out session information and make observations
- Review 1st of each quarter to update total sessions

**Column explanations:**

- Type: helps identify the frequency of which certain types of information is shared on our current blog
- Bracket: helps quickly sort blog posts by performance level
- Category: indicates class of information
- Total sessions in month: how many sessions the post received in the month it was published
- Audience: indicates who we expect to reach
- Theme: indicates the structure of the post
- Topic: indicates the main subject covered

### Crediting blog posts

Add [a note](/handbook/product/technical-writing/markdown-guide/#note) at the end of a blog post reading "[Name] contributed to this story/post" if:

- You ghostwrote the post on behalf of someone else who is listed as the author
- You were heavily involved in editing/structuring/rewriting the post authored by someone else
- You wrote the post, but it is a company announcement listing GitLab as the author
- You wrote the post and are listed as the author, but wish to give credit to interviewees

## Key Responsibilities

At the highest level, Content Marketing is responsible for building awareness, trust, and preference for the GitLab brand with developers, engineers, and IT professionals.

1. Audience development
2. Editorial voice and style
3. Defining and executing the quarterly content strategy
4. Lead generation

## Campaigns<a name="campaigns"></a>
Content marketing is responsible for executing marketing campaigns for GitLab. We define a campaign as any programmed interaction with a user, customer, or prospect.  For each campaign, we will create a campaign brief that outlines the overall strategy, goals, and plans for the campaign.

## Campaign Brief Process
To create a campaign brief, first start with the "campaign brief template" (which can be found by searching on the google drive). Fill out all fields in the brief as completely as possible. Certain fields might not be applicable to a particular campaign. For example, an email nurture campaign leveraging text based emails won’t have a visual design component. This field can be left blank in that example.

Once the campaign brief is filled out, create an issue in the GitLab Marketing project and link to the campaign brief.

On the GitLab issue, make sure to:
* Tag all stakeholders
* Use the Marketing Campaign label
* Set the appropriate due date (the due date should be the campaign launch date)
* If there are specific deliverables, create a todo list in the issue description for each stakeholder along with a due date

### Defining Social Media Sharing Information

Social Media Sharing info is set by the post or page frontmatter, by adding two variables:

```yaml
description: "short description of the post or page"
twitter_image: '/images/tweets/image.png'

## Ensuring your Post Will Have a Functional Card and Image

When you post a link on Facebook or Twitter, either you can see only a link, or a full interactive card, which displays information about that link: title, **description**, **image** and URL.

For Facebook these cards are configured via [OpenGraph Meta Tags][OG]. Twitter Cards were recently set up for our website as well.

Please compare the following images illustrating post's tweets.

A complete card will look like this:

![Twitter Card example - complete][twitter-card-comp]

An incomplete card will look like this:

![Twitter Card example - incomplete][twitter-card-incomp]

Note that the [first post] has a **specific description** and the image is a **screenshot** of the post's cover image, taken from the [Blog landing page][blog]. This screenshot can be taken locally when previewing the site at `localhost:4567/blog/`.

```

This information is valid for the entire website, including all the webpages for about.GitLab.com, handbook, and blog posts.

#### Images

All the images or screenshots for `twitter_image` should be pushed to the [www-gitlab-com] project at `/source/images/tweets/` and must be named after the page's file name.

For the second post above, note that the tweet image is the blog post cover image itself, not the screenshot. Also, there's no `description` provided in the frontmatter, so our Twitter Cards and Facebook's post will present the _fall back description_, which is the same for all about.GitLab.com.

For the handbook, make sure to name it so that it's obvious to which handbook it refers. For example, for the Marketing Handbook, the image file name is `handbook-marketing.png`. For the Team Handbook, the image is called `handbook-gitlab.png`. For Support, it would be named `handbook-support.png`, and so on.

#### Description

The `description` meta tag [is important][description-tag]
for SEO, also is part of [Facebook Sharing][og] and [Twitter Cards]. We set it up in the
[post or page frontmatter](../blog/#frontmatter), as a small summary of what the post is about.

The description is not meant to repeat the post or page title, use your creativity to describe the content of the post or page.
Try to use about 70 to 100 chars in one sentence.

As soon as you add both description and social sharing image to a page or post, you must check and preview them with the [Twitter Card Validator]. You can also verify how it looks on the FB feed with the [Facebook Debugger].

#### Examples

To see it working, you can either share the page on Twitter or Facebook, or just test it with the [Twitter Card Validator].

- Complete post, with `description` and `twitter_image` defined:
[GitLab Master Plan](/2016/09/13/gitlab-master-plan/)
- Incomplete post, with only the `description` defined:
[Y Combinator Post](/2016/09/30/gitlabs-application-for-y-combinator-winter-2015/)
- Incomplete post, with none defined: [8.9 Release](/2016/06/22/gitlab-8-9-released/)
- Page with both defined: [Marketing Handbook](/handbook/marketing/)
- Page with only `twitter_image` defined: [Team Handbook](/handbook/)
- Page with none defined: [Blog landing page](/blog/)


<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->
<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
